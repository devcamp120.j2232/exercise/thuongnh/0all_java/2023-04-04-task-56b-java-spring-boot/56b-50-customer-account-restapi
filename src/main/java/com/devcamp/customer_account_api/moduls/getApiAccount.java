package com.devcamp.customer_account_api.moduls;



import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin   //Java @CrossOrigin: cho phép CORS trên RESTful web service.
@RestController
public class getApiAccount {
    
        @GetMapping("/accounts")
        public ArrayList<Account> splitString(@RequestParam(value="inputString", defaultValue = "mot hai ba bon") String input) {
            // tạo ra 3 đối tượng khách hàng 
            Customer customer1 = new Customer(1, "thuong", 20);
            Customer customer2 = new Customer(2, "duong", 50);
            Customer customer3 = new Customer(3, "luong", 90);
            // khởi tạo 3 đối tượng tài khoản
            Account account1 = new Account(1,customer1,120 );
            Account account2 = new Account(2,customer2,130 );
            Account account3 = new Account(3,customer3,140 );
            // khởi tạo đối tượng ARR
            ArrayList<Account> accountARR = new ArrayList<Account>();
            // thêm các đối tượng vào mảng
            Account[] arrString = {account1, account2, account3};
            // thêm đối tượng vào arrlisat
            accountARR = new ArrayList<Account>(Arrays.asList(arrString));
            return accountARR;
        }
        
}
